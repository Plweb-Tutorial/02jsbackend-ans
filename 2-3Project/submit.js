var querystring = require('querystring');
var http = require('http');
var path = require("path");

var webdriver = require('selenium-webdriver');
//var By = webdriver.By;
var chrome = require('selenium-webdriver/chrome');

var chromepath = require('chromedriver').path;
var driver = chrome.Driver.createSession(
    new chrome.Options(), 
    new chrome.ServiceBuilder(chromepath).build());

var result = [];

checkLanding();
checkAboutus();
checkIngredient();
checkRegister();
checkLogin(sentdata);

function checkLanding(){
  driver.get('localhost:3000');
  driver.getTitle().then(function(title) {
    if(title === "PLSHOP")
      result.push("Success");
    else
      result.push("Fail");
  });
}

function checkAboutus(){
  driver.get('localhost:3000/aboutus');
  driver.getTitle().then(function(title) {
    if(title === "PLSHOP")
      result.push("Success");
    else
      result.push("Fail");
  });
}

function checkIngredient(){
  driver.get('localhost:3000/ingredient');
  driver.getTitle().then(function(title) {
    if(title === "PLSHOP")
      result.push("Success");
    else
      result.push("Fail");
  });
}

function checkRegister(){
  driver.get('localhost:3000/register');
  driver.getTitle().then(function(title) {
    if(title === "PLSHOP")
      result.push("Success");
    else
      result.push("Fail");
  });
}

function checkLogin(){
  driver.get('localhost:3000/login');
  driver.getTitle().then(function(title) {
    if(title === "PLSHOP")
      result.push("Success");
    else
      result.push("Fail");
    json();
  });
  driver.quit();
}

function json()
{
  var fs = require('fs');
   
  fs.readFile('C:/PLweb-win32-x64/resources/app/bat/key.txt', function (err, data) {
      if (err) 
      {
        console.log('err'+err);
      }
      else{
    sentdata(data);
      }
  });
}

function sentdata(key){
  var state = true;
  let keys =String(key);
  console.log(result);

  for(var i=0; i<result.length; i++){
    if(result[i]=="Fail"){
      state = false;
      break;
    }
  }

  const postData = querystring.stringify({
    'id': path.parse(path.join(__dirname, "..")).name,
    'exercise': path.parse(__dirname).name,
    'key':keys,
    'state': state
  });

   const options = {
    hostname: '140.125.90.232',
    port: 3000,
    path: '/ProjectIDE/compiler',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(postData)
    }
  };
  
  const req = http.request(options, 
  function (res){
    //console.log(`STATUS: ${res.statusCode}`);
    //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');

    res.on('data', 
    function (chunk) {
      //console.log(`BODY: ${chunk}`);
    });
    
    res.on('end', 
    function(){
      console.log('提交成功');
    });
  });
  
  req.on('error', 
  function(e) {
    console.error(`提交失敗: ${e.message}`);
  });

  req.write(postData);
  console.log(querystring.parse(postData));
  req.end();
}
