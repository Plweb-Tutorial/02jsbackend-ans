const PORT = process.env.PORT || 3000;

var express = require("express");
var path = require("path");
var app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, 'views'));
app.use(express.static(__dirname + "/public"));

app.get("/", function(req, res){
  res.render("index");
});

app.get("/page1", function(req, res){
  res.render("page1");
});

app.get("/page2", function(req, res){
  res.render("page2");
});

app.listen(PORT, function(){
  console.log("Starting the server port " + PORT);
});